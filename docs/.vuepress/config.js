module.exports = {
  serviceWorker: 'true',
  title: "Amy Storm Kosman",
  description: "creative",
  head: [
    'link',
    {
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '57x57',
      href: '/apple-touch-icon-57x57.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '114x114',
      href: '/apple-touch-icon-114x114.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '72x72',
      href: '/apple-touch-icon-72x72.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '144x144',
      href: '/apple-touch-icon-144x144.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '60x60',
      href: 'apple-touch-icon-60x60.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '120x120',
      href: 'apple-touch-icon-120x120.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '76x76',
      href: 'apple-touch-icon-76x76.png'
    },
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '152x152',
      href: 'apple-touch-icon-152x152.png'
    },
    {
      rel: 'icon',
      type: 'image/png',
      href: 'favicon-196x196.png',
      sizes: '196x196'
    },
    {
      rel: 'icon',
      type: 'image/png',
      href: 'favicon-96x96.png',
      sizes: '96x96'
    },
    {
      rel: 'icon',
      type: 'image/png',
      href: 'favicon-32x32.png',
      sizes: '32x32'
    },
    {
      rel: 'icon',
      type: 'image/png',
      href: 'favicon-16x16.png',
      sizes: '16x16'
    },
    {
      rel: 'icon',
      type: 'image/png',
      href: 'favicon-128.png',
      sizes: '128x128'
    }
  ],
  themeConfig: {
    logo: '/logo.png',
    nav: [{
        text: 'Home',
        link: 'https://www.asknv.askcreative.space/',
        external: 'false'
      },
      {
        text: 'About',
        link: '/about/'
      },
      {
        text: 'Blog',
        link: '/blog/'
      },
      {
        text: 'Contact',
        link: '/contact/'
      },
      {
        text: 'Gallery',
        link: 'https://www.asknv.askcreative.space/gallery',
        external: 'false'
      },
      {
        text: 'Info',
        link: '/info/'
      },
      {
        text: 'Shop',
        link: 'https://www.asknv.askcreative.space/shop',
        external: 'false'
      }
    ],
    sidebar: {
      '/info/': [
        '',
        'cookies',
        'payments',
        'privacy',
        'returns',
        'shipping',
        'terms'
      ]
    },
    lastUpdated: 'Last Updated', // string | boolean
  }
}
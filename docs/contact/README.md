---
sidebar: auto
---
  <section class="hero">
        <h1 class="fancyh1 title is-size-1">
        Contact
        </h1>
        <h2 class="subtitle is-size-3">
        Many ways to get in touch with Amy.
        </h2>
  </section>
  
:::tip
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="60" viewBox="0 0 20 20"><path d="M18.777 5.084l-3-2c-0.082-0.055-0.179-0.084-0.277-0.084h-4.5v-1.5c0-0.827-0.673-1.5-1.5-1.5h-2c-0.827 0-1.5 0.673-1.5 1.5v1.5h-5.5c-0.276 0-0.5 0.224-0.5 0.5v4c0 0.276 0.224 0.5 0.5 0.5h5.5v1h-1.5c-0.099 0-0.195 0.029-0.277 0.084l-3 2c-0.139 0.093-0.223 0.249-0.223 0.416s0.084 0.323 0.223 0.416l3 2c0.082 0.055 0.179 0.084 0.277 0.084h1.5v5.5c0 0.276 0.224 0.5 0.5 0.5h4c0.276 0 0.5-0.224 0.5-0.5v-5.5h3.5c0.276 0 0.5-0.224 0.5-0.5v-4c0-0.276-0.224-0.5-0.5-0.5h-3.5v-1h4.5c0.099 0 0.195-0.029 0.277-0.084l3-2c0.139-0.093 0.223-0.249 0.223-0.416s-0.084-0.323-0.223-0.416zM7 1.5c0-0.276 0.224-0.5 0.5-0.5h2c0.276 0 0.5 0.224 0.5 0.5v1.5h-3v-1.5zM10 19h-3v-5h3v5zM14 13h-9.349l-2.25-1.5 2.25-1.5h9.349v3zM10 9h-3v-1h3v1zM15.349 7h-14.349v-3h14.349l2.25 1.5-2.25 1.5z" fill="#fefcfb"></path></svg>
  
## Mailing Address
#### Amy Storm Kosman creative
##### Mirastraat 13
##### 7521ZE Enschede, NL
:::
  
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=6.845630407333375%2C52.23182602420405%2C6.850887537002564%2C52.23386629434296&amp;layer=mapnik&amp;marker=52.23284617099531%2C6.848258972167969" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=52.23285&amp;mlon=6.84826#map=19/52.23285/6.84826">View Larger Map</a></small>
  
:::tip
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="60" viewBox="0 0 20 20"><path d="M17.894 8.897c-1.041 0-2.928-0.375-3.516-0.963-0.361-0.361-0.446-0.813-0.515-1.177-0.085-0.448-0.136-0.581-0.332-0.666-0.902-0.388-2.196-0.61-3.551-0.61-1.34 0-2.62 0.219-3.512 0.6-0.194 0.083-0.244 0.216-0.327 0.663-0.068 0.365-0.152 0.819-0.512 1.179-0.328 0.328-1.015 0.554-1.533 0.685-0.668 0.169-1.384 0.267-1.963 0.267-0.664 0-1.113-0.126-1.372-0.386-0.391-0.391-0.641-0.926-0.685-1.467-0.037-0.456 0.051-1.132 0.68-1.762 1.022-1.022 2.396-1.819 4.086-2.368 1.554-0.506 3.322-0.773 5.114-0.773 1.804 0 3.587 0.27 5.156 0.782 1.705 0.556 3.093 1.361 4.124 2.393 1.050 1.050 0.79 2.443 0.012 3.221-0.257 0.257-0.7 0.382-1.354 0.382zM9.98 4.481c1.507 0 2.908 0.246 3.946 0.691 0.713 0.306 0.833 0.938 0.92 1.398 0.052 0.275 0.097 0.513 0.24 0.656 0.252 0.252 1.706 0.671 2.809 0.671 0.481 0 0.633-0.082 0.652-0.094 0.31-0.314 0.698-1.086-0.017-1.802-1.805-1.805-5.010-2.882-8.574-2.882-3.535 0-6.709 1.065-8.493 2.848-0.288 0.288-0.42 0.616-0.391 0.974 0.025 0.302 0.17 0.614 0.39 0.836 0.019 0.012 0.173 0.098 0.67 0.098 1.098 0 2.541-0.411 2.789-0.659 0.141-0.141 0.185-0.379 0.236-0.654 0.086-0.462 0.203-1.095 0.917-1.4 1.026-0.439 2.413-0.68 3.905-0.68z" fill="#fefcfb"></path><path d="M16.5 18h-13c-0.671 0-1.29-0.264-1.743-0.743s-0.682-1.112-0.645-1.782c0.004-0.077 0.118-1.901 1.27-3.739 0.682-1.088 1.586-1.955 2.686-2.577 1.361-0.769 3.020-1.159 4.932-1.159s3.571 0.39 4.932 1.159c1.101 0.622 2.005 1.489 2.686 2.577 1.152 1.839 1.266 3.663 1.27 3.739 0.037 0.67-0.192 1.303-0.645 1.782s-1.072 0.743-1.743 0.743zM10 9c-3.117 0-5.388 1.088-6.749 3.233-1.030 1.623-1.139 3.282-1.14 3.299-0.022 0.392 0.111 0.761 0.373 1.038s0.623 0.43 1.017 0.43h13c0.393 0 0.754-0.153 1.017-0.43s0.395-0.646 0.373-1.039c-0.001-0.016-0.111-1.675-1.14-3.298-1.362-2.145-3.633-3.233-6.749-3.233z" fill="#fefcfb"></path><path d="M10 16c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3-1.346 3-3 3zM10 11c-1.103 0-2 0.897-2 2s0.897 2 2 2c1.103 0 2-0.897 2-2s-0.897-2-2-2z" fill="#fefcfb"></path></svg>

## Telephone
##### [+31 06-20707292](tel:310620707292 "+31 0620707292")
:::

:::tip
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="60" viewBox="0 0 20 20"><path d="M17.5 6h-16c-0.827 0-1.5 0.673-1.5 1.5v9c0 0.827 0.673 1.5 1.5 1.5h16c0.827 0 1.5-0.673 1.5-1.5v-9c0-0.827-0.673-1.5-1.5-1.5zM17.5 7c0.030 0 0.058 0.003 0.087 0.008l-7.532 5.021c-0.29 0.193-0.819 0.193-1.109 0l-7.532-5.021c0.028-0.005 0.057-0.008 0.087-0.008h16zM17.5 17h-16c-0.276 0-0.5-0.224-0.5-0.5v-8.566l7.391 4.927c0.311 0.207 0.71 0.311 1.109 0.311s0.798-0.104 1.109-0.311l7.391-4.927v8.566c0 0.276-0.224 0.5-0.5 0.5z" fill="#fefcfb"></path></svg>
## Email Address
##### [hello@askcreative.space](mailto:hello@askcreative.space "hello@askcreative.space")
:::

:::tip
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="60" height="60" viewBox="0 0 20 20"><path d="M12.496 16.925c-0.111 0-0.379-0.037-0.552-0.369-0.058-0.111-0.104-0.251-0.142-0.429l-2.36-11.013-2.196 8.053c-0.177 0.648-0.545 0.765-0.749 0.776s-0.583-0.062-0.832-0.686l-1.164-2.911-0.164 0.411c-0.279 0.697-1.085 1.243-1.836 1.243h-2c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h2c0.346 0 0.779-0.293 0.907-0.614l0.257-0.643c0.243-0.607 0.628-0.697 0.836-0.697s0.593 0.091 0.836 0.697l1.075 2.686 2.344-8.596c0.175-0.642 0.5-0.769 0.744-0.762s0.561 0.151 0.7 0.802l2.285 10.662 1.33-6.652c0.133-0.664 0.491-0.801 0.693-0.823s0.581 0.032 0.856 0.651l1.188 2.672c0.144 0.323 0.596 0.617 0.949 0.617h2c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5h-2c-0.743 0-1.561-0.532-1.863-1.211l-0.955-2.149-1.495 7.477c-0.036 0.178-0.080 0.319-0.137 0.431-0.17 0.338-0.442 0.376-0.55 0.377-0.001 0-0.002 0-0.003 0z" fill="#fefcfb"></path></svg>
## Amy Storm Kosman, Owner
![](https://res.cloudinary.com/askcreative-space/image/upload/h_150,c_fit/AmyCVpic_ocwe7o.jpg)
##### [amy@askcreative.space](mailto:amy@askcreative.space "amy@askcreative.space")
:::

:::tip
## & More...

|![diaspora logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771216/diaspora_p40nlj.png "Diaspora")|![vkontakte logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,c_scale,f_auto,w_57/v1525771216/vkontakte_tmfz6x.png "VKontakte")|![facebook logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771215/facebook_synael.png "Facebook")|![twitter logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771216/twitter_srdflh.png "Twitter")|
|---|---|---|---|
|![flickr logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771217/flickr_pvhyvk.png "Flickr")|![odnoklassniki logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771217/odnoklassniki_znjd9t.png "Odnoklassniki")|![github logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771217/github_czrubb.png "Github")|
|![linkedin logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771215/linkedin_ztbsae.png "LinkedIn")|![google-plus logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771215/google-plus_toa2hh.png "Google+")|![line logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771215/line_wabidq.png "Line")|![instagram logo](https://res.cloudinary.com/askcreative-space/image/upload/c_scale,f_auto,w_57/v1525771215/instagram_disbz6.png "Instagram")|
:::

<MyBottom />
---
sidebar: auto
---
  <section class="hero">
        <h1 class="fancyh1 title is-size-1">
          Blog
        </h1>
        <h2 class="subtitle is-size-3">
          All my boring stories and fascinating interests.
        </h2>
        <br>
  </section>
  
<BlogIndex />

<MyBottom />